<?php
class Main
{
    public static function start($file, $name)
    {
        echo Core::showAction('start', $file);
        echo Core::showAction('head', $name);
    }

    public static function stop($file)
    {
        echo Core::showAction('stop', $file);
    }
}

