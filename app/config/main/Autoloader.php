<?php

class Autoloader {

    public static function register() {
        try
        {
            spl_autoload_register(function ($class) {
                foreach (APP_DIRS as $folder) {
                    $file = APP_CONF . $folder . $class . ".php";

                    if (file_exists($file)) {
                        require_once($file);
                        break;
                    }
                }
            });
        } catch (Exception $except) {
            echo "Произошла ошибка:\n" . $except->getMessage();
        }
    }
}

Autoloader::register();
