<?php
class Route
{
    public static function start()
    {
        echo Core::showAction('start', __FILE__);
        $work = true;
        $argv = Core::getArgv();
        $msgStart = '';
        $command = isset($argv[1]) ? $argv[1] : '';
        // убираем элемент [1], оставляя остальной массив для аргументов следующего скрипта
        Core::spliceArgv(1, 1);
        

        if (!$command)
        {
            die("Команда отсутствует\nСкрипт завершён.\n");
        }

        while ($work)
        {
            if (!isset($command))
            {
                echo "Введите команду:\n";
                $command = Core::getStock();
            }

            echo "подключаем скприпт ..\n";

            switch ($command)
            {
                case '1':
                case 'loop_array':
                    LoopArray::main();
                    break;
                case '2':
                case 'bubble_sort':
                    BubbleSort::main();
                    break;
                case '3':
                case 'frequent_repetition':
                    FrequentRepetition::main();
                    break;
                case '4':
                case 'polindrom':
                    Polindrom::main();
                    break;

                default:
                    $msgStart = "Команда не выбрана или такой команды нет\n";
            }
            
            echo "{$msgStart}Продолжить выполнение? Да / Нет? Введите: [[y] / [n]]\n> ";
            $nextCommand = Core::getStock();

            echo "Введено> " . $nextCommand . "\n";
            $work = $nextCommand === 'y' ? true : false;
            Core::resetArgv();
            unset($command);
        }

        echo Core::showAction('stop', __FILE__);
    }
}
