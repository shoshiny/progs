<?php
/**
 * Основной класс по получению и обработке информации в приложении
 */
class Core
{
    /**
     * получение аргументов запуска скрипта из консоли
     * php myscrip.php argv1 argv2 argv3 ...
     */
    public static function getArgv($number = null)
    {
        if (!is_null($number) && is_numeric($number))
        {
            return $_SERVER["argv"][$number] ?? '';
        }

        return $_SERVER["argv"] ?? '';
    }

    /**
     * удаление заданного элемента из массива параметров скрипта
     */
    public static function spliceArgv($delElem, $count)
    {
        array_splice($_SERVER['argv'], $delElem, $count);
    }

    /**
     * обнуление массива параметра скрипта
     */
    public static function resetArgv()
    {
        unset($_SERVER['argv']);
    }

    /**
     * получение аргументов из консоли запросом
     */
    public static function getStock()
    {
        return trim(fgets(STDIN));
    }

    /**
     * получение имени файла из пути
     */
    public static function getScriptFileName($script)
    {
        $name = explode(DIRECTORY_SEPARATOR, $script);

        return $name[count($name) - 1];
    }

    /**
     * показ сообщения в зависимости от действия
     */
    public static function showAction($action, $script)
    {
        $message = '';

        switch ($action)
        {
            case 'start':
                $message = "Запуск скрипта " . self::getScriptFileName($script) . "\n";
                break;
            case 'stop':
                $message = "Завершение скрипта " . self::getScriptFileName($script) . "\n";
                break;
            case 'head':
                $message = self::showHeadScript($script);
                break;
        }

        return $message;
    }

    /**
     * отображение в консоли название выполняемого скрипта
     */
    public static function showHeadScript($msg)
    {
        $count = mb_strlen($msg); // в отличии от strlen() - возвращает длину строки, а не количество байт

        return str_repeat("*", $count) . "\n{$msg}\n" . str_repeat("*", $count) . "\n";
    }
    
    /**
     * парсим строку ввода и формируем из неё массив
     */
    public static function getDataInCli($params = [])
    {
        if (isset($params["firstText"]))
        {
            echo $params["firstText"] . "\n";
        }

        if (isset($params["formatText"]))
        {
            echo $params["formatText"] . "\n";
        }

        // запрос ввода данных
        $data = Core::getStock();

        if (isset($params["enterText"]) && $params["enterText"])
        {
            echo "Введено> " . $data . "\n";
        }

        return $data;
    }

    /**
     * получение подстроки из строки [1, 3, 6, 7]
     * и её сжатие
     */
    public static function getSubString($inputText)
    {
        $pattern = '/^\[(.*)\]/i';
        preg_match_all($pattern, $inputText, $matchs);

        if (isset($matchs[1][0]))
        {
            $matchs[1][0] = str_replace(" ", "", $matchs[1][0]);
        }

        return [
            $matchs[0][0],
            $matchs[1][0]
        ];
    }

    /**
     * получение массива из строки [1, 3, 6, 7] command number
     */
    public static function getArrFromInput($inputText)
    {
        $resultArr = self::getSubString($inputText);

        return isset($resultArr[1]) ? explode(",", $resultArr[1]) : [];
    }

    /**
     * преобразование ввода массива с пробелами в массив без пробелов + остаток строки
     */
    public static function prepareArr($inputText)
    {
        $matchs = self::getSubString($inputText);

        return '[' . $matchs[1] . ']' . explode($matchs[0], $inputText)[1];
    }

    public static function getParamsArgv()
    {
        $firstElemArr = '';
        $firstElem = $lastElem = true;
        $argv = Core::getArgv() ? Core::getArgv() : [];

        foreach ($argv as $key => $elem)
        {
            if ($key > 0)
            {
                if ($firstElem)
                {
                    if (strstr($elem, "["))
                    {
                        $firstElem = false;
                    }

                    $firstElemArr .= $elem;
                } elseif ($lastElem) {
                    if (strstr($elem, "]"))
                    {
                        $lastElem = false;
                    }

                    $firstElemArr .= $elem;
                } else {
                    $firstElemArr .= ' ' . $elem;
                }
            }
        }

        return $firstElemArr;
    }

}
