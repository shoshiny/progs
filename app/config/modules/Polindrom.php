<?php

class Polindrom extends Main implements Modules
{
    public static function check_polindrom($inputString)
    {
        $inputString = str_replace(" ", "", mb_strtolower($inputString));
        $count = mb_strlen($inputString);
        $exit = true;
        $string = [];

        // сохраняем слово посимвольно в массив, для следующего шага
        // без этого шага дальше может начаться побитовое сравнение вместо посимвольного
        for ($i = 0; $i < $count; $i++)
        {
            $string[] = mb_substr($inputString, 0, 1);
            $inputString = mb_substr($inputString, 1);
        }

        // начинаем сравнение
        for ($i = 0; $i < $count; $i++)
        {
            if ($string[$i] != $string[$count - 1 - $i])
            {
                $exit = false;
                break;
            }
        }

        return $exit ? "Это полиндром!" : "Нет, это не полиндром!";
    }

    public static function main()
    {
        self::start(__FILE__, "Задача о слове-полиндроме");

        // получение входных данных
        $inputString = !empty(Core::getArgv()) && count(Core::getArgv()) < 3 ? Core::getArgv(1) : Core::getParamsArgv();

        if (empty($inputString))
        {
            $param = [
                "firstText"     => "Введите строку для определения является ли она полиндромом",
                "formatText"    => "формат ввода: aaaabb или bbbaaabaaaa",
                "enterText"     => true
            ];
            $inputString = Core::getDataInCli($param);
        } else {
            echo "Введено> " . $inputString . "\n";
        }

        echo self::check_polindrom($inputString) . "\n";

        self::stop(__FILE__);
    }
}
