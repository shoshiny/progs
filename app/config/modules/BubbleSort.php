<?php
/**
 * Сортировка пузырьком вверх и вниз
 */
class BubbleSort extends Main implements Modules
{
    /**
     * на входе произвольный массив, на выходе отсортированный
     */
    public static function buble_sort($arraySort)
    {
        $count = count($arraySort);

        for ($i = 0; $i < $count - 1; $i++)
        {
            $stopFlag = true;

            for ($j = 0; $j < $count - 1 - $i; $j++)
            {
                $temp = $arraySort[$j + 1];

                if ($arraySort[$j] > $arraySort[$j + 1])
                {
                    $arraySort[$j + 1] = $arraySort[$j];
                    $arraySort[$j] = $temp;
                }

                $stopFlag = false;
            }

            if ($stopFlag)
            {
                break;
            }
        }

        return $arraySort;
    }

    public static function main()
    {
        self::start(__FILE__, 'Сортировка пузырьком');

        // получить входные значения
        $inputString = !empty(Core::getArgv()) && count(Core::getArgv()) < 4 ? Core::getArgv(1) : Core::getParamsArgv();

        if (empty($inputString))
        {
            $param = [
                "firstText"     => "Введите массив для сортировки",
                "formatText"    => "формат ввода: [1, 4, 8, 2, 3]", // top или [1, 4, 8, 2, 3] down
                "enterText"     => true
            ];
            $inputString = Core::getDataInCli($param);
        } else {
            echo "Введено> " . $inputString . "\n";
        }

        $inputArr = Core::getArrFromInput($inputString);
        $sortArr = self::buble_sort($inputArr);
        echo "Результат> [" . implode(", ", $sortArr) . "] \n";

        self::stop(__FILE__);
    }

}
