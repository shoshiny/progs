<?php
/**
 * Сдвиг данных внутри массива вправо и влево
 */
class LoopArray extends Main implements Modules
{
    /**
     * формирование входных данных
     */
    public static function getString()
    {
        $argv = Core::getArgv();
    
        if (count($argv) == 3)
        {
            if (!empty($argv[1]) && !empty($argv[2]) && !empty($argv[3]))
            {
                return $argv[1] . ' ' . $argv[2] . ' ' . $argv[3];
            }
        } elseif (count($argv) > 4) {
            return Core::getParamsArgv();
        }

        return false;
    }

    /**
     * функция "прокрутки" влево или вправо массива на указанное количество
     */
    public static function loop_array($data, $command, $number)
    {
        $newData = [];
        $count = count($data);
        $newData = array_merge($data, $data);

        if (!in_array($command, ['left', 'right']))
        {
            $message = "Ошибка входных данных. Команда {$command} не соответствует left или right!\n";
        }

        if ($number > $count)
        {
            $number = $number % $count;
        }

        if ($command == 'left')
        {
            // берем начало массива
            $newData = array_slice($newData, $number, $count);
        } elseif ($command == 'right') {
            // берем конец массива
            $newData = array_slice($newData, $count - $number, $count);
        }

        $message = "Результат> [" . implode(", ", $newData) . "]\n";

        return $message;
    }

    /**
     * основная функция скрипта
     */
    public static function main() {
        self::start(__FILE__, 'Старт скприпта работы по прокручиванию массива "влево" или "вправо"');
        $number = null;

        $command = "";
        $matchs = $arrData = [];
        // запускаем сбор данных
        $line = self::getString();

        if (empty($line) || $line === false)
        {
            $params = [
                "firstText"     => "Введите массив, команду и значение:",
                "formatText"    => "формат ввода: [1, 2, 3] left 2",
                "enterText"     => true
            ];
            $line = Core::getDataInCli($params);
        }

        $line = Core::prepareArr($line);
        // преобразование строки ввода в массив для работы с ним
        $commandLine = !empty($line) ? explode(" ", $line) : [];

        // data для loop_array()
        if (isset($commandLine[0]))
        {
            $pattern = '/^\[(.*)\]/i';
            preg_match_all($pattern, $commandLine[0], $matchs);

            if (isset($matchs[1][0]))
            {
                $matchs[1][0] = str_replace(" ", "", $matchs[1][0]);
                $arrData = explode(",", $matchs[1][0]);
            }
        }

        // command для loop_array()
        if (isset($commandLine[1]))
        {
            $command = trim($commandLine[1]);
        }

        // number для loop_array()
        if (isset($commandLine[2]))
        {
            $number = trim($commandLine[2]);
        }

        echo self::loop_array($arrData, $command, $number);
        self::stop(__FILE__);
    }

}
