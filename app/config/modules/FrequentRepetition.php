<?php

class FrequentRepetition extends Main implements Modules
{
    /**
     * вычисление символа наиболее часто встречающегося в строке
     */
    public static function frequent_repetition($inputString)
    {
        $result = $preResult = [];

        echo "входящая строка > " . $inputString . "\n";

        // начинаем посимвольный разбор строки
        $inputArr = str_split($inputString);
        echo "элементов в строке > " . mb_strlen($inputString) . "\n";
        $countString = mb_strlen($inputString);

        for ($i = 0; $i < $countString; $i++)
        {
            $key = mb_substr($inputString, 0, 1);
            $inputString = mb_substr($inputString, 1);
            $preResult[$key] = isset($preResult[$key]) ? $preResult[$key] + 1 : 1;
        }

        $max = max(array_values($preResult));

        // производим формирование результата
        foreach ($preResult as $key => $val)
        {
            if ($val == $max)
            {
                $result[] = [$key, $val];
            }
        }

        // формируем строку вывода
        $preResult = "";

        foreach ($result as $key => $arr)
        {
            $preResult .= "[" . implode(", ", $arr) . "] ";
        }

        $result = $preResult . "\n";

        // находим максимальные значения в массиве результатов
        echo "элементов строки преобразованной в массив > " . count($inputArr) . "\n";

        return $result;
    }

    /**
     * запуск основного скрипта
     */
    public static function main()
    {
        self::start(__FILE__, 'Самый частый символ');

        $inputString = !empty(Core::getArgv()) && count(Core::getArgv()) < 4 ? Core::getArgv(1) : Core::getParamsArgv();

        if (empty($inputString))
        {
            $param = [
                "firstText"     => "Введите строку в которой будут подсчитываться символы",
                "formatText"    => "формат ввода: aaaabb или bbbaaabaaaa или cbdeuuu900", // top или [1, 4, 8, 2, 3] down
                "enterText"     => true
            ];
            $inputString = Core::getDataInCli($param);
        } else {
            echo "Введено> " . $inputString . "\n";
        }

        echo self::frequent_repetition($inputString);

        self::stop(__FILE__);
    }
}
