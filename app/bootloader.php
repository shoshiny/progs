<?php
/**
 * скрипт создан для работы в консоли CLI
 * определяем среду и завершаем работу, если это не консоль
 */
if (php_sapi_name() == 'cli')
{
    define("CLI", true);
}

if (!defined("CLI") && CLI)
{
    die("Скрипт запущен не в консоле!\n");
}

require_once __DIR__ . "/config.php";
require_once APP_CONF . APP_MAIN . "Autoloader.php";
Route::start();
