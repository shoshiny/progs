<?php
/**
 * папки приложения
 */
define('APP_CONF',      __DIR__ . '/config/');
define('APP_INTERFACE', 'interface/');
define('APP_MAIN',      'main/');
define('APP_MODULES',   'modules/');
// загрузка классов из папок
define(
    'APP_DIRS',
    [
        APP_INTERFACE,
        APP_MAIN,
        APP_MODULES
    ]
);
